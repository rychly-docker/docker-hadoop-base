#!/bin/sh

. $(dirname "${0}")/install.sh
. $(dirname "${0}")/set-pax-flag-of-java.sh
. $(dirname "${0}")/application-helpers.sh

# the functions require GNU sed (the usage of busybox sed results into incorrect outputs)

addHadoopProperty() {
	local path="${1}"
	local name="${2}"
	local value="${3}"
	local entry="<property><name>${name}</name><value>${value}</value></property>"
	local escapedEntry=$(echo "${entry}" | sed 's/\//\\\//g')
	sed -i "/<\/configuration>/ s/.*/${escapedEntry}\n&/" "${path}"
}

cleanHadoopProperties() {
	local path="${1}"
	sed -i '/^<configuration>/,/^<\/configuration>/{//!d}' "${path}"
}

reconfHadoopByEnvVars() {
	local path="${1}"
	local envPrefix="${2}"
	[ -e "${path}" ] || return
	echo "* reconfiguring ${path}"
	cleanHadoopProperties "${path}"
	for I in `printenv | grep "^${envPrefix}_[^=]*="`; do
		local name=`echo "${I}" | sed -e "s/^${envPrefix}_\\([^=]*\\)=.*\$/\\1/" -e 's/___/-/g' -e 's/__/_/g' -e 's/_/./g'`
		local value="${I#*=}"
		echo "** setting ${name}=${value}"
		addHadoopProperty "${path}" "${name}" "${value}"
	done
}

reconfHadoopByEnvVars "${HADOOP_CORE_CONF}" PROP_CORE
reconfHadoopByEnvVars "${HADOOP_HDFS_CONF}" PROP_HDFS
reconfHadoopByEnvVars "${HADOOP_YARN_CONF}" PROP_YARN
reconfHadoopByEnvVars "${HADOOP_MAPRED_CONF}" PROP_MAPRED
reconfHadoopByEnvVars "${HADOOP_HTTPFS_CONF}" PROP_HTTPFS
reconfHadoopByEnvVars "${HADOOP_KMS_CONF}" PROP_KMS
