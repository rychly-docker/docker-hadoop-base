#!/bin/sh

# This script is sourced to another scripts, so there cannot be any exits, overrides of variables of function names, etc.

if [ "${1}" = "--help" ]; then
	echo "Usage: INSTALL_PKGS='[packages ...]' $0" >&2
	echo "Install distribution packages of names given in the environment variable INSTALL_PKGS (a space separated list of the packages)." >&2
	return 1
fi

packages_check() {
	[ $# -lt 1 ] && return 0
	case "${PM}" in
		apt-get)
			dpkg-query -s $@ >/dev/null 2>&1
			return $?
		;;
		apk)
			COUNT=$(apk list -qI $@ | wc -l)
			[ "${COUNT}" -eq $# ] && return 0 || return 1
		;;
	esac
}

packages_update() {
	case "${PM}" in
		apt-get) apt-get update ;;
		apk) apk update ;;
	esac
}

packages_install() {
	case "${PM}" in
		apt-get) DEBIAN_FRONTEND=noninteractive apt-get -y install $@ ;;
		apk) apk add --no-cache $@ ;;
	esac
}

packages_clean() {
	case "${PM}" in
		apt-get) apt-get clean && rm -rf /var/lib/apt/lists/* 2>/dev/null ;;
		apk) rm -rf /var/cache/apk/* 2>/dev/null ;;
	esac
}

packages_main() {
	[ -z "${INSTALL_PKGS}" ] && return
	if which apt-get >/dev/null 2>&1; then
		PM=apt-get
	elif which apk >/dev/null 2>&1; then
		PM=apk
	else
		echo "Unknown package manager! Supported managers: apt-get, apk" >&2
		return 2
	fi
	if ! packages_check ${INSTALL_PKGS}; then
		packages_update
		packages_install ${INSTALL_PKGS}
		packages_clean
	fi
}

# Do not use cmd args as they are args of an entrypoint script that is sourcing this install script,
# i.e., they are not args for this install scripts
# [ -z "${INSTALL_PKGS}" ] && local INSTALL_PKGS=$@

packages_main
