# Reverse Dependencies

After rebuild of this Docker image, it is necessary to rebuild also the following Docker images:

*	docker-hdfs
*	docker-spark
*	docker-yarn
